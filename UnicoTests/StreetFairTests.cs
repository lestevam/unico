using Moq;
using NUnit.Framework;
using Unico.Interfaces;
using Unico.Services;
using Unico.Models;
using Unico.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace UnicoTests
{
    /// <summary>
    /// Responsible class by street fair service tests
    /// </summary>
    [TestFixture]
    public class StreetFairTests
    {
        private IStreetFairService _streetFairService;
        private Mock<IStreetFairRepository> _iStreetFairRepository;
        private Mock<ILogService> _iLogService;
        private List<StreetFair> StreetFairs { get; set; }
        private StreetFair newStreetFair { get; set; }

        /// <summary>
        /// Initial setup for all test cases
        /// </summary>
        [SetUp]
        public void Setup()
        {
            StreetFairs = new List<StreetFair>
            {
                new StreetFair()
                {
                    id = 1
                    , registro = "1087-1"
                    , regiao5 = "Sul"
                    , distrito = "PEDREIRA"
                    , nome_feira = "MAR PAULISTA"
                    , bairro = "PEDREIRA"
                }
                ,new StreetFair()
                {
                    id = 2
                    , registro = "1040-5"
                    , regiao5 = "Norte"
                    , distrito = "FREGUESIA DO O"
                    , nome_feira = "CRUZ DAS ALMAS"
                    , bairro = "CRUZ DAS ALMAS"
                }
                ,new StreetFair()
                {
                    id = 3
                    , registro = "3008-2"
                    , regiao5 = "Oeste"
                    , distrito = "PERDIZES"
                    , nome_feira = "ALTO DAS PERDIZES"
                    , bairro = "PERDIZES"
                }
            };

            _iLogService = new Mock<ILogService>(MockBehavior.Default);
            _iStreetFairRepository = new Mock<IStreetFairRepository>(MockBehavior.Default);
            _streetFairService = new StreetFairService(
                _iStreetFairRepository.Object,
                _iLogService.Object
            );
        }

        [TestCase]
        public void OnGetAll_ShouldReturnAllStreetFairs()
        {
            // Arrange
            _iStreetFairRepository.Setup(_ => _.GetAll()).Returns(StreetFairs);

            // Act
            var streetFairs = _streetFairService.getAll();

            //Assert
            Assert.GreaterOrEqual(streetFairs.AsQueryable().Count(),1);
        }

        [TestCase]
        public void OnGetById_WhenPassedId_ShouldReturnOneStreetFair()
        {
            // Arrange
            var id = 1;
            _iStreetFairRepository.Setup(_ => _.GetById(id)).Returns(StreetFairs.Where(x => x.id == id).First);
            _iStreetFairRepository.Setup(_ => _.isExists(It.IsAny<int>())).Returns(true);

            // Act
            var streetFair = _streetFairService.get(id);

            //Assert
            Assert.AreEqual(streetFair.id, 1);
        }

        [TestCase]
        public void OnUpdate_WhenChangeValue_ShouldChangeElement()
        {
            // Arrange
            newStreetFair = setNewStreetFair();
            StreetFairs.Add(newStreetFair);
            newStreetFair.registro = "888-1";
            StreetFair result = null;

            _iStreetFairRepository.Setup(_ => _.GetById(It.IsAny<int>())).Returns(StreetFairs.Where(x => x.id == newStreetFair.id).First);
            _iStreetFairRepository.Setup(_ => _.Save(It.IsAny<StreetFair>())).Callback<StreetFair>(r => result = r);
            _iStreetFairRepository.Setup(_ => _.isExists(It.IsAny<int>())).Returns(true);

            // Act
            _streetFairService.update(newStreetFair);
            var streetFair = _streetFairService.get(newStreetFair.id);

            //Assert
            Assert.AreEqual(streetFair.id, 4);
            Assert.AreEqual(streetFair.registro, "888-1");
        }

        [TestCase]
        public void OnExists_WhenExistsId_ShouldReturnTrue()
        {
            // Arrange
            var id = 1;
            _iStreetFairRepository.Setup(_ => _.isExists(It.IsAny<int>())).Returns(StreetFairs.Any(x => x.id == id));

            // Act
            var result = _streetFairService.isExists(id);

            //Assert
            Assert.AreEqual(true, result);
        }

        [TestCase]
        public void OnExists_WhenNotExistsId_ShouldReturnFalse()
        {
            // Arrange
            var id = 5;
            _iStreetFairRepository.Setup(_ => _.isExists(It.IsAny<int>())).Returns(StreetFairs.Any(x => x.id == id));

            // Act
            var result = _streetFairService.isExists(id);

            //Assert
            Assert.AreEqual(false, result);
        }

        [TestCase]
        public void OnDelete_WhenPasseId_ShouldDeleteElement()
        {
            // Arrange
            newStreetFair = setNewStreetFair();
            StreetFairs.Add(newStreetFair);

            _iStreetFairRepository.Setup(_ => _.isExists(It.IsAny<int>())).Returns(StreetFairs.Any(x => x.id == newStreetFair.id));
            _iStreetFairRepository.Setup(_ => _.Delete(It.IsAny<int>())).Callback(() => StreetFairs.RemoveAt(StreetFairs.IndexOf(newStreetFair)));

            // Act
            _streetFairService.delete(newStreetFair.id);

            //Assert
            Assert.AreEqual(3, StreetFairs.Count);
        }


        [TestCase]
        public void OnInsert_WhenPasseId_ShouldInsertElement()
        {
            // Arrange
            newStreetFair = setNewStreetFair();
            _iStreetFairRepository.Setup(_ => _.Insert(It.IsAny<StreetFair>())).Callback(() => StreetFairs.Add(newStreetFair));

            // Act
            _streetFairService.insert(newStreetFair);

            //Assert
            Assert.AreEqual(4, StreetFairs.Count);
        }


        [TestCase]
        public void OnGetBySearchTypeRegiao5_WhenPassedTypeAndText_ShouldReturnAllElementsFound()
        {
            // Arrange
            newStreetFair = setNewStreetFair();
            StreetFairs.Add(newStreetFair);
            var searchText = "Leste";
            _iStreetFairRepository.Setup(_ => _.GetBy(StreetFairSearchTypes.REGIAO5, It.IsAny<string>())).Returns(StreetFairs.Where(x => x.regiao5.ToLower().Contains(searchText.ToLower())));

            // Act
            var streetFairs = _streetFairService.getBy(StreetFairSearchTypes.REGIAO5, searchText);

            //Assert
            Assert.GreaterOrEqual(streetFairs.AsQueryable().Count(), 1);
        }

        public void OnGetBySearchTypeDistrito_WhenPassedTypeAndText_ShouldReturnAllElementsFound()
        {
            // Arrange
            newStreetFair = setNewStreetFair();
            StreetFairs.Add(newStreetFair);
            var searchText = "Leste";
            _iStreetFairRepository.Setup(_ => _.GetBy(StreetFairSearchTypes.REGIAO5, It.IsAny<string>())).Returns(StreetFairs.Where(x => x.regiao5.ToLower().Contains(searchText.ToLower())));

            // Act
            var streetFairs = _streetFairService.getBy(StreetFairSearchTypes.REGIAO5, searchText);

            //Assert
            Assert.GreaterOrEqual(streetFairs.AsQueryable().Count(), 1);
        }

        private StreetFair setNewStreetFair()
        { 
            return new StreetFair() { id = 4, registro = "999-1", regiao5 = "Leste", distrito = "PENHA", nome_feira = "MARIA CARLOTA", bairro = "PENHA" };
        }
    }
}
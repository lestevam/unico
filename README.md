# Unico

## Projeto

O projeto foi desenvolvido com C# e foi utilizado as seguintes tecnologias: ASP.NET API, Entity Framework, AutoFac, Powershell, NLog, Cake, OpenCover, ReportGenerator, GhostDoc, JQuery, Bootstrap, e SQLite.

---

## Banco de dados e importação de dados do CSV 

O banco de dados é composto por um arquivo de nome "Unico.db" proveniente do banco de dados SQLite e está localizado na pasta App_Data do projeto ("Unico/App_Data").  
As configurações fornecidas no arquivo web.config já são suficientes para o projeto ter acesso ao banco.

Para exportar os dados do CSV, por favor, utilize o arquivo "ImportCSV.ps1" localizado na pasta raiz do projeto Unico.  
Este arquivo solicita como parâmetro o caminho completo do arquivo CSV. Importante lembrar que o script faz a leitura seguindo o modelo indicado, se houver alguma diferença de colunas pode ocorrer erro na execução do script.

Exemplo:

```
	ImportCsv.ps1 -FileCsv C:\arquivo.csv
```

O script realizará o processo de importação e no final mostrará um resumo do que foi importado.

---

## Documentação

Para documentação foi utilizado o GhostDoc Community (https://submain.com/ghostdoc/).  
O arquivo gerado está na pasta "Documentation" da raiz da solução:

Documentation\Unico.chm

---

## Cobertura de testes

O relatório de cobertura de testes utiliza o Cake para osquestrar os passos e OpenCover e ReportGenerator para gerar o relatório.  
O relatório está disponibilizado também na pasta "Documentation" da raiz da solução:
Neste mesmo diretório existe o arquivo UnicoReport.xml que é gerado automaticamente no build do projeto e que é utilizado para gerar o relatório.

Documentation\CoverReport\index.htm

Para gerar a cobertura de testes é necessário executar o arquivo "build.ps1" que está localizado na pasta raiz da solução.  
Este arquivo utiliza o build.cake que é o arquivo que contém os passos necessários para gerar o relatório conforme mencionado acima.

---

## Logs

A geração de log é realizada pelo NLog e o arquivo será disponibilizado na raiz do projeto "Unico" quando for executado.

Unico\unico.log

---

## CRUD, API e Exemplos

Foi desenvolvido uma página com um CRUD simples e que não utiliza todos os campos disponíveis de dados, porém tem o intuíto de mostrar o uso da API. Está página deve ser visivel ao iniciar a execução do projeto.

Os métodos disponiveis na API são:

- https://localhost/api/StreetFair/{id} - [GET, DELETE ]
- https://localhost/api/StreetFair/ - [ GET, POST, PUT ]
- https://localhost/api/StreetFair/{searchType} - [nomefeira, regiao5, distrito, bairro, id]

### CRUD
![Scheme](https://bitbucket.org/lestevam/unico/raw/master/images/crud.png)

### GETALL

![Scheme](https://bitbucket.org/lestevam/unico/raw/master/images/getall.png)

### GETBYID

![Scheme](https://bitbucket.org/lestevam/unico/raw/master/images/getbyid.png)

### GETBYTYPE

![Scheme](https://bitbucket.org/lestevam/unico/raw/master/images/getbytype.png)

### UPDATE

![Scheme](https://bitbucket.org/lestevam/unico/raw/master/images/update.png)

### INSERT

![Scheme](https://bitbucket.org/lestevam/unico/raw/master/images/insert.png)

### DELETE

![Scheme](https://bitbucket.org/lestevam/unico/raw/master/images/delete.png)
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;
using Unico.Interfaces;
using Unico.Models;
using Unico.Services;

namespace Unico.Infrastructure
{
    /// <summary>
    /// Generic Repository
    /// </summary>
    /// <typeparam name="T">Type</typeparam>
    /// <seealso cref="Unico.Interfaces.IRepository&lt;T&gt;" />
    public class GenericRepository<T> : IRepository<T> where T : class
    {
        private Context db = new Context();
        private ILogService _logService;

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericRepository{T}"/> class.
        /// </summary>
        /// <param name="logService">The log service.</param>
        public GenericRepository(ILogService logService) 
        {
            _logService = logService;
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(object id)
        {
            _logService.Info($"Deleting ID: {id}");
            var entity = db.Set<T>().Find(new object[] { id });
            db.Entry(entity).State = System.Data.Entity.EntityState.Deleted;
            db.SaveChanges();
            _logService.Info("Deleted");
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<T> GetAll()
        {
            _logService.Info($"Get all {typeof(T)}");
            return db.Set<T>().ToList();
        }

        /// <summary>
        /// Gets the by filter.
        /// </summary>
        /// <param name="expression">The expression.</param>
        /// <returns></returns>
        public IEnumerable<T> GetByFilter(Expression<Func<T, bool>> expression)
        {
            _logService.Info($"Get {typeof(T)} by expression {expression.ToString()}");
            return db.Set<T>().Where(expression);
        }

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public T GetById(object id)
        {
            _logService.Info($"Get {typeof(T)} with ID: {id}");
            return db.Set<T>().Find(new object[] { id });
        }

        /// <summary>
        /// Inserts the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public void Insert(T entity)
        {
            _logService.Info($"Inserting {typeof(T)}: {Newtonsoft.Json.JsonConvert.SerializeObject(entity)}");
            db.Set<T>();
            db.Entry(entity).State = System.Data.Entity.EntityState.Added;
            db.SaveChanges();
            _logService.Info("Added");
        }

        /// <summary>
        /// Determines whether the specified identifier is exists.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        ///   <c>true</c> if the specified identifier is exists; otherwise, <c>false</c>.
        /// </returns>
        public bool isExists(object id)
        {
            _logService.Info($"Checking if exists ID: {id}");
            var entity = db.Set<T>().Find(new object[] { id });
            return entity == null ? false : true;
        }

        /// <summary>
        /// Saves the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public void Save(T entity)
        {
            _logService.Info($"Update {typeof(T)} ID: {entity.GetType().GetProperty("id").GetValue(entity)}");
            Type t = entity.GetType();
            PropertyInfo prop = t.GetProperty("id");
            var e = db.Set<T>().Find(new object[] { prop.GetValue(entity) });
            db.Entry(e).CurrentValues.SetValues(entity);
            db.SaveChanges();
            _logService.Info($"Updated: {Newtonsoft.Json.JsonConvert.SerializeObject(entity)}");
        }
    }
}
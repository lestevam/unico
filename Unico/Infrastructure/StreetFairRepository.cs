﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Unico.Enums;
using Unico.Interfaces;
using Unico.Models;

namespace Unico.Infrastructure
{
    /// <summary>
    /// Street Fair Repository
    /// </summary>
    /// <seealso cref="Unico.Infrastructure.GenericRepository&lt;Unico.Models.StreetFair&gt;" />
    /// <seealso cref="Unico.Interfaces.IStreetFairRepository" />
    public class StreetFairRepository : GenericRepository<StreetFair>, IStreetFairRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StreetFairRepository"/> class.
        /// </summary>
        /// <param name="logService">The log service.</param>
        public StreetFairRepository(ILogService logService) : base(logService) { }

        /// <summary>
        /// Gets the by.
        /// </summary>
        /// <param name="searchType">Type of the search.</param>
        /// <param name="searchText">The search text.</param>
        /// <returns></returns>
        public IEnumerable<StreetFair> GetBy(StreetFairSearchTypes searchType, string searchText)
        {
            searchText = searchText?.ToLower() ?? "";
            switch (searchType)
            {
                case StreetFairSearchTypes.BAIRRO:
                    return this.GetByFilter(x => x.bairro.ToLower().Contains(searchText));
                case StreetFairSearchTypes.DISTRITO:
                    return this.GetByFilter(x => x.distrito.ToLower().Contains(searchText));
                case StreetFairSearchTypes.NOMEFEIRA:
                    return this.GetByFilter(x => x.nome_feira.ToLower().Contains(searchText));
                case StreetFairSearchTypes.REGIAO5:
                    return this.GetByFilter(x => x.regiao5.ToLower().Contains(searchText));
                default:
                    return null;
            }
            
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.SQLite;

namespace Unico.Models
{
    public class Context : DbContext
    {
        public Context() : base("connSqlite") 
        {
            
        }
        public DbSet<StreetFair> StreetFairs { get; set; }
        
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Unico.Models
{
    public class StreetFair
    {
        [Key]
        public int id { get; set; }
        public double longitude { get; set; }
        public double latitude { get; set; }
        public string setcens { get; set; }
        public string areap { get; set; }
        public int coddist { get; set; }
        public string distrito { get; set; }
        public int codsubpref { get; set; }
        public string sufprefe { get; set; }
        public string regiao5 { get; set; }
        public string regiao8 { get; set; }
        public string nome_feira { get; set; }
        public string registro { get; set; }
        public string logradouro { get; set; }
        public string numero { get; set; }
        public string bairro { get; set; }
        public string referencia { get; set; }

    }
}
﻿param(
    [Parameter(Mandatory = $true)][string]$FileCsv
)


function Main {
    $separator = ("#" * 120)
    Write-Host $separator
    Write-Host "Reading CSV File: $FileCsv"
    Write-Host $separator
    $csv = Get-Csv
    Invoke-ExportCsvToDataBase -csv $csv
    Write-Host "Finished"
    Write-Host $separator
}

function Get-Csv{
    return Import-Csv -Delimiter "," -Path $FileCsv
}

function Invoke-ExportCsvToDataBase{
    param(
        [Parameter(Mandatory=$true)][Object[]]$csv
    )
    Write-Host "Exporting to database"
    $con = Open-Connection
    Invoke-CreateTable -con $con
    Invoke-InsertRow -con $con -csv $csv
    Get-Data -con $con
    Close-Connection -con $con
}

function Invoke-CreateTable{
    param(
        [Parameter(Mandatory=$true)][System.Data.SQLite.SQLiteConnection]$con
    )

    Write-Host "Creating table"
    $sql = $con.CreateCommand()
    $sql.CommandText = "DROP TABLE IF EXISTS StreetFairs"
    $null = $sql.ExecuteNonQuery()
    
    $sql.CommandText= @"
            CREATE TABLE 
               StreetFairs 
            (
                id integer PRIMARY KEY AUTOINCREMENT NOT NULL
                , longitude decimal(18,2)
                , latitude decimal(18,2)
                , setcens varchar(15)
                , areap varchar(15)
                , coddist int
                , distrito varchar(18)
                , codsubpref int
                , sufprefe varchar(25)
                , regiao5 varchar(6)
                , regiao8 varchar(7)
                , nome_feira varchar(30)
                , registro varchar(6)
                , logradouro varchar(34)
                , numero varchar(5)
                , bairro varchar(20)
                , referencia varchar(24)
            )
"@
                    
    $null = $sql.ExecuteNonQuery()
    Write-Host "Table created"
}

function Invoke-InsertRow{
    param(
        [Parameter(Mandatory=$true)][System.Data.SQLite.SQLiteConnection]$con,
        [Parameter(Mandatory=$true)][Object[]]$csv
    )

    Write-Host "Inserting rows"
    $sql = $con.CreateCommand()
    $rowsCount = 0
    $csv | ForEach-Object {
        if ($_.REGISTRO){
            $cmdText=@"
                INSERT INTO 
                    StreetFairs 
                    (
                        longitude
                        , latitude
                        , setcens
                        , areap
                        , coddist
                        , distrito
                        , codsubpref
                        , sufprefe
                        , regiao5
                        , regiao8
                        , nome_feira
                        , registro
                        , logradouro
                        , numero
                        , bairro
                        , referencia
                    )
                VALUES 
                    (
                        $($_.LONG)
                        ,$($_.LAT)
                        ,'$($_.SETCENS.replace("`'","''"))'
                        ,'$($_.AREAP.replace("`'","''"))'
                        ,$([int]$_.CODDIST)
                        ,'$($_.DISTRITO.replace("`'","''"))'
                        ,$([int]$_.CODSUBPREF)
                        ,'$($_.SUBPREFE.replace("`'","''"))'
                        ,'$($_.REGIAO5.replace("`'","''"))'
                        ,'$($_.REGIAO8.replace("`'","''"))'
                        ,'$($_.NOME_FEIRA.replace("`'","''"))'
                        ,'$($_.REGISTRO.replace("`'","''"))'
                        ,'$($_.LOGRADOURO.replace("`'","''"))'
                        ,'$($_.NUMERO.replace("`'","''"))'
                        ,'$($_.BAIRRO.replace("`'","''"))'
                        ,'$( if ($_.REFERENCIA -ne $null ) { $_.REFERENCIA.replace("`'","''") })'
                    )
"@
            #Write-Host $cmdText
            $sql.CommandText = $cmdText
            $null = $sql.ExecuteNonQuery()
            $count = ++$rowsCount
            Write-Host "Adding row: $($count)"
        }
    }   
    Write-Host "Inserting of rows finished"
}

function Get-Data{
    param(
        [Parameter(Mandatory=$true)][System.Data.SQLite.SQLiteConnection]$con
    )

    Write-Host "Getting data inserteds"
    $sql = $con.CreateCommand()
    $sql.CommandText = "SELECT * FROM StreetFairs"
    $adapter = New-Object -TypeName System.Data.SQLite.SQLiteDataAdapter $sql
    $data = New-Object System.Data.DataSet
    [void]$adapter.Fill($data)
    (0..$($data.tables[0].Rows.Count)) | foreach{$data.tables[0].Rows[$_]} | out-gridview -Title 'StreetFairs'
}

function Open-Connection{
    Add-Type -Path "$PSScriptRoot\App_Data\System.Data.SQLite.dll"
    $con = New-Object -TypeName System.Data.SQLite.SQLiteConnection
    $con.ConnectionString = "Data Source=$PSScriptRoot\App_Data\unico.db" 
    $con.Open()
    return $con
}

function Close-Connection{
    param(
        [Parameter(Mandatory=$true)][System.Data.SQLite.SQLiteConnection]$con
    )

    $con.Close()
}


Main
﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Unico.Interfaces;
using Unico.Models;
using Unico.Infrastructure;
using System.Diagnostics;
using Unico.Enums;

namespace Unico.Services
{
    /// <summary>
    /// Service for street fairs.
    /// </summary>
    /// <seealso cref="Unico.Interfaces.IStreetFairService" />
    public class StreetFairService : IStreetFairService
    {
        private readonly IStreetFairRepository _streetFairRepository;
        private readonly ILogService _logService;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="StreetFairService"/> class.
        /// </summary>
        /// <param name="streetFairRepository">The street fair repository.</param>
        /// <param name="logService">The log service.</param>
        public StreetFairService(IStreetFairRepository streetFairRepository, ILogService logService)
        {
            _streetFairRepository = streetFairRepository;
            _logService = logService;
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<StreetFair> getAll()
        {
            return _streetFairRepository.GetAll();
        }

        /// <summary>
        /// Gets the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">ID not found.</exception>
        public StreetFair get(int id)
        {
            if (isExists(id))
                return _streetFairRepository.GetById(id);
            else
                throw new ArgumentException("ID not found.");
        }

        /// <summary>
        /// Updates the specified street fair.
        /// </summary>
        /// <param name="streetFair">The street fair.</param>
        public void update(StreetFair streetFair)
        {
            _streetFairRepository.Save(streetFair);
        }

        /// <summary>
        /// Determines whether the specified identifier is exists.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        ///   <c>true</c> if the specified identifier is exists; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="System.ArgumentException">ID not found</exception>
        public bool isExists(int id)
        {
            return id <= 0 || !_streetFairRepository.isExists(id) ? false : true;       
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void delete(int id)
        {
            if (isExists(id))
                _streetFairRepository.Delete(id);
            else
                throw new ArgumentException("ID not found");
        }
        
        /// <summary>
        /// Inserts the specified street fair.
        /// </summary>
        /// <param name="streetFair">The street fair.</param>
        public void insert(StreetFair streetFair)
        {
            _streetFairRepository.Insert(streetFair);
        }

        /// <summary>
        /// Gets the by.
        /// </summary>
        /// <param name="searchType">Type of the search.</param>
        /// <param name="searchText">The search text.</param>
        /// <returns>IEnumerable with street fairs found</returns>
        public IEnumerable<StreetFair> getBy(StreetFairSearchTypes searchType, string searchText)
        {
            return _streetFairRepository.GetBy(searchType, searchText);
        }
    }
}
﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Unico.Interfaces;

namespace Unico.Services
{
    /// <summary>
    /// Log Service
    /// </summary>
    /// <seealso cref="Unico.Interfaces.ILogService" />
    public class LogService : ILogService
    {
        private Logger log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString());

        /// <summary>
        /// Informations the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Info(object message)
        {
            log.Info(message);
        }
    }
}
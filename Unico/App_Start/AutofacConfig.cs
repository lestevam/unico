﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Autofac;
using Unico.Services;
using Unico.Interfaces;
using Unico.Controllers;
using System.Web.Http;
using Autofac.Integration.WebApi;
using Unico.Infrastructure;

namespace Unico
{
    public class AutofacConfig
    {
        public static void RegisterTypes()
        {
            var builder = new ContainerBuilder();
            builder.RegisterInstance(new LogService()).As<ILogService>();
            builder.RegisterType<StreetFairService>().As<IStreetFairService>();
            builder.RegisterType<StreetFairRepository>().As<IStreetFairRepository>();
            builder.RegisterType<StreetFairController>();
            var container = builder.Build();
            var webApiResolver = new AutofacWebApiDependencyResolver(container);
            GlobalConfiguration.Configuration.DependencyResolver = webApiResolver;
        }
    }
}
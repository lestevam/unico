﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Unico.Enums;
using Unico.Models;

namespace Unico.Interfaces
{
    public interface IStreetFairService
    {
        IEnumerable<StreetFair> getAll();
        StreetFair get(int id);
        void update(StreetFair streetFair);
        bool isExists(int id);
        void delete(int id);
        void insert(StreetFair streetFair);
        IEnumerable<StreetFair> getBy(StreetFairSearchTypes searchType, string searchText);

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unico.Interfaces
{
    public interface ILogService
    {
        void Info(object message);
    }
}

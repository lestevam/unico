﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Unico.Interfaces
{
    public interface IRepository<T> where T : class 
    {
        IEnumerable<T> GetAll();
        T GetById(object id);
        void Save(T entity);
        void Insert(T entity);
        void Delete(object id);
        IEnumerable<T> GetByFilter(Expression<Func<T, bool>> expression);
        bool isExists(object id);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unico.Models;
using Unico.Enums;

namespace Unico.Interfaces
{
    public interface IStreetFairRepository : IRepository<StreetFair>
    {
        IEnumerable<StreetFair> GetBy(StreetFairSearchTypes searchType, string searchText);
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Unico.Models;
using System.Web;
using Newtonsoft.Json;
using NLog;
using Unico.Services;
using Unico.Interfaces;

namespace Unico.Controllers
{
    /// <summary>
    /// Street Fair API
    /// </summary>
    public class StreetFairController : ApiController
    {
        private IStreetFairService _service;

        /// <summary>
        /// Initializes a new instance of the <see cref="StreetFairController"/> class.
        /// </summary>
        /// <param name="streetFairService">The street fair service.</param>
        public StreetFairController(IStreetFairService streetFairService)
        {
            this._service = streetFairService;
        }
        
        /// <summary>
        /// Get all street fairs
        /// </summary>
        /// <returns>IEnumerable with all street fairs</returns>
        public IEnumerable<StreetFair> GetAllStreetFairs() 
        {
            return _service.getAll();
        }

        /// <summary>
        /// Gets the stret fair.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public IHttpActionResult GetStretFair(int id) 
        {
            var streetFair = _service.get(id);
            if (streetFair == null) return NotFound();
            return Ok(streetFair);
        }

        /// <summary>
        /// Puts the specified street fair.
        /// </summary>
        /// <param name="streetFair">The street fair.</param>
        /// <returns></returns>
        public IHttpActionResult Put(StreetFair streetFair)
        {
            if (streetFair.id <= 0 || !_service.isExists(streetFair.id)) return NotFound();
            _service.update(streetFair);
            return Ok(streetFair);
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public IHttpActionResult Delete(int id)
        {
            if (id <= 0 || !_service.isExists(id)) return NotFound();
                _service.delete(id);
            
            return Ok();
        }

        /// <summary>
        /// Posts the specified street fair.
        /// </summary>
        /// <param name="streetFair">The street fair.</param>
        /// <returns></returns>
        public IHttpActionResult Post(StreetFair streetFair)
        {
            _service.insert(streetFair);
            return Ok();
        }

        /// <summary>
        /// Gets the by regiao5.
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/StreetFair/regiao5")]
        public IEnumerable<StreetFair> GetByRegiao5(string searchText) 
        {
            return _service.getBy(Enums.StreetFairSearchTypes.REGIAO5, searchText);
        }

        /// <summary>
        /// Gets the by distrito.
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/StreetFair/distrito")]
        public IEnumerable<StreetFair> GetByDistrito(string searchText)
        {
            return _service.getBy(Enums.StreetFairSearchTypes.DISTRITO, searchText);
        }

        /// <summary>
        /// Gets the by nome feira.
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/StreetFair/nomefeira")]
        public IEnumerable<StreetFair> GetByNomeFeira(string searchText)
        {
            return _service.getBy(Enums.StreetFairSearchTypes.NOMEFEIRA, searchText);
        }

        /// <summary>
        /// Gets the by bairro.
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/StreetFair/bairro")]
        public IEnumerable<StreetFair> GetByBairro(string searchText)
        {
            return _service.getBy(Enums.StreetFairSearchTypes.BAIRRO, searchText);
        }

    }
}

Task("BuildTest")
    .Does(() => 
    {
        MSBuild("./UnicoTests/UnicoTests.csproj", 
            new MSBuildSettings {
                Verbosity = Verbosity.Minimal,
                Configuration = "Debug"
            }
        );
    }); 
	
#tool "nuget:?package=OpenCover"
#tool "nuget:?package=NUnit.ConsoleRunner"
 
Task("OpenCover")
    .IsDependentOn("BuildTest")
    .Does(() => 
    {
        var openCoverSettings = new OpenCoverSettings()
        {
            Register = "user",
            SkipAutoProps = true,
            ArgumentCustomization = args => args.Append("-coverbytest:*Tests.dll").Append("-mergebyhash")
        };
 
        var outputFile = new FilePath("./Documentation/UnicoReport.xml");
 
        OpenCover(tool => {
                var testAssemblies = GetFiles("./UnicoTests/bin/Debug/net472/UnicoTests.dll");
                tool.NUnit3(testAssemblies);
            },
            outputFile,
            openCoverSettings
                .WithFilter("+[Unico*]*")
                .WithFilter("-[UnicoTests]*")
        );
    });
	
#tool nuget:?package=ReportGenerator&version=4.8.12
#addin nuget:?package=Cake.Powershell&version=1.0.1
	
Task("PowerShell")
    .IsDependentOn("OpenCover")
    .Does(() => 
    {
			StartPowershellScript("./tools/ReportGenerator.4.8.12/tools/net47/ReportGenerator.exe -reports:./Documentation/UnicoReport.xml -targetdir:./Documentation/CoverReport");
	});

RunTarget("PowerShell");